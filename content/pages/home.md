---
title: A sweet new programming language
---

[patreon]: https://patreon.com/jneen "Contribute to Tulip development"
[source]: https://gitlab.com/tulip-lang/tulip
[intro]: https://gitlab.com/tulip-lang/tulip/blob/master/doc/intro.md
[snek]: http://snek.jneen.net/
[twitter]: https://twitter.com/tuliplang

[**We need your help to make tulip a reality!**][patreon]

Tulip is a work-in-progress programming language that's going to be super cool. To get started, watch our strangeloop talk:

<iframe width="480" height="270" src="https://www.youtube.com/embed/lvclTCDeIsY" frameborder="0" allowfullscreen></iframe>

In case you can't watch the video right now, here's a little example to whet your appetite:

``` tulip
@impl map f (xs:.cons _ _) = map-list f xs
@impl map f .nil = .nil

map-list f xs = {
  map-step (.nil) out = out
  map-step (.cons x xs) out = map-step xs (.cons (f x) out)
  map-step xs .nil > reverse
}
```

For more info, you can:

* Read the [pitch][patreon]
* Read the [intro post][intro]
* Browse or download the [source][source]
* Join [snek][] and chat in the `#tulip` channel
  - *Note: if the inviter isn't working, send an email to jneen at jneen dot net and i'll invite you manually*
* Follow [@tuliplang][twitter] on twitter

<3 <3 <3 <3 <3

--Jeanine
